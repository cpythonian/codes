import sqlite3
import pandas as pd

class FireShotDatabase:
    def __init__(self):
        self.__database_name = 'Fireshot'

    def create_table(self):
        farmer_query = """
        CREATE TABLE IF NOT EXISTS farmers
        (id, farmer_name TEXT, mobile TEXT, city TEXT, land_size TEXT)
        """

        supply_query = """
        CREATE TABLE IF NOT EXISTS supply
        (user_id INTEGER, product TEXT, quantity INTEGER, price REAL)
        """

        customer_query = """
        CREATE TABLE IF NOT EXISTS customers
        (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, mobile TEXT, address TEXT) 
        """

        customer_orders = """
        CREATE TABLE IF NOT EXISTS orders
        (id INTEGER PRIMARY KEY AUTOINCREMENT, customer_id INTEGER, farmer_id INTEGER, product TEXT, quantity INTEGER)
        """

        conn = sqlite3.connect(self.__database_name)
        cursor = conn.cursor()
        cursor.execute(farmer_query)
        cursor.execute(supply_query)
        cursor.execute(customer_query)
        cursor.execute(customer_orders)

        conn.commit()
        conn.close()

    def __validate_data(self, req):
        data = req
        for key, value in data.items():
            if key == 'phone-number':
                try:
                    ast.literal_eval(value)
                except Exception as E:
                    data[key] = 'invalid_format'
            elif key == 'acres':
                try:
                    ast.literal_eval(value)
                except Exception as E:
                    data[key] = 'invalid_format'
            elif key == 'quantity':
                try:
                    data[key] = re.findall('\d+', value)[0]
                except Exception as E:
                    data[key] = 'invalid_format'
            elif key == 'price':
                data[key] = data[key]['amount']
            elif key == 'land_size':
                try:
                    data[key] = ast.literal_eval(value)
                except Exception as E:
                    data[key] = 0

        return data

    def insert(self, req):
        user_id = self.get_user_id()
        clean_data = self.__validate_data(req)

        query = """
        INSERT INTO farmers
        VALUES({}, '{}', '{}', '{}', '{}')
        """.format(user_id, clean_data['name'], clean_data['phone-number'],
                   clean_data['geo-city'], clean_data['acres'])

        conn = sqlite3.connect(self.__database_name)
        cursor = conn.cursor()
        cursor.execute(query)
        conn.commit()
        conn.close()

    def get_user_id(self):
        query = """
        SELECT * FROM farmers
        """
        conn = sqlite3.connect(self.__database_name)
        data = pd.read_sql(query, conn)
        conn.close()
        return data['id'].max()+1



# if __name__ == '__main__':
#     obj = FireShotDatabase()
#     obj.create_table()
#     obj.insert()