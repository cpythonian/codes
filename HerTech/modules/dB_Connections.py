import sqlite3
import ast
import re
import pandas as pd


class FireShot:
    def __init__(self):
        self.__database = './modules/Fireshot'

    def __execute_query(self, query, parameter, vals=None):
        conn = sqlite3.connect(self.__database)

        if parameter == 'INSERT':
            cur = conn.cursor()
            cur.execute(query)
            print(query)
            return 'inserted'
        elif parameter == 'SELECT':
            data = pd.read_sql(query, conn)
            return data

        conn.commit()
        conn.close()

    def __validate_data(self, req):
        data = req
        for key, value in data.items():
            if key == 'phone-number':
                try:
                    ast.literal_eval(value)
                except Exception as E:
                    data[key] = 'invalid_format'
            elif key == 'acres':
                try:
                    ast.literal_eval(value)
                except Exception as E:
                    data[key] = 'invalid_format'
            elif key == 'quantity':
                try:
                    data[key] = re.findall('\d+', value)[0]
                except Exception as E:
                    data[key] = 'invalid_format'
            elif key == 'price':
                data[key] = data[key]['amount']
            elif key == 'land_size':
                try:
                    data[key] = ast.literal_eval(value)
                except Exception as E:
                    data[key] = 0

        return data

    def save_farmer_data(self, req):
        id_query = """
        SELECT * FROM farmers
        """
        new_user_id = self.__execute_query(id_query, 'SELECT')['id'].max()
        clean_data = self.__validate_data(req)

        vals = (new_user_id + 1,
                   clean_data['name'], clean_data['phone-number'],
                   clean_data['geo-city'], clean_data['acres'])
        print(vals)
        insert_query = """
        INSERT INTO farmers('id', 'farmer_name', 'mobile', 'city', 'land_size')
        VALUES({}, '{}', '{}', '{}', '{}')
        """.format(new_user_id + 1,
                   clean_data['name'], clean_data['phone-number'],
                   clean_data['geo-city'], clean_data['acres'])

        self.__execute_query(insert_query, 'INSERT', vals)
