import sqlite3
import pandas as pd

conn = sqlite3.connect('./modules/Farmers')
def insert_table():
    cur = conn.cursor()
    farmers = [
        ('Shubham Jante', 'green apple', 100, 130.0, 'kashmir'),
        ('Shubham Jante', 'regular apple', 100, 80.0, 'kashmir'),
        ('Shubham Jante', 'oranges', 100, 30.0, 'kashmir'),
        ('Yash Agrawal', 'oranges', 70, 40.0, 'nagpur'),
        ('Yash Agrawal', 'green apple', 300, 110.0, 'nagpur'),
    ]
    cur.executemany('INSERT INTO supply VALUES (?, ?, ?, ?, ?)', farmers)
    conn.commit()
    # conn.close()

def create_table():
    query = """
    CREATE TABLE IF NOT EXISTS supply
    (farmer_name TEXT, product TEXT, quantity INT, unit_price REAL, location TEXT)
    """

    cur = conn.cursor()
    cur.execute(query)


def display_table():
    query = """
    SELECT farmer_name from supply
    WHERE product='green apple'
    AND location='kashmir'
    AND quantity>=100
    """

    print(pd.read_sql(query, conn))


if __name__ == '__main__':
    create_table()
    insert_table()
    display_table()