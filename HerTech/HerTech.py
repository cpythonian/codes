from flask import Flask, request, render_template
from modules.dB_Connections import FireShot
import sqlite3
import pandas as pd

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/ivr')
def ivr():
    return render_template('IVR.html')

@app.route('/sample', methods=['POST'])
def sample():
    req = request.get_json()
    print(req['queryResult'])

    conn = sqlite3.connect('./modules/Farmers')
    # data = farm
    return 'done'



if __name__ == '__main__':
    app.run(port='8080', debug=True)
